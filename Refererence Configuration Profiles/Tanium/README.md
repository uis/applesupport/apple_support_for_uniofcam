# Reference Configuration Profiles for Tanium

This profile must be installed via MDM to devices with the Supervised status.

## PPPC for Tanium
Provides the necessary permissions for the Tanium applications: Tanium Client, Tanium CLient Extensions, Tanium End-user Notifications.
The PPPC custom payload must be delivered using a User-Approved MDM (UAMDM) payload in a device profile.
From https://help.tanium.com/bundle/ug_client_cloud/page/client/deployment_client_management.html

## Managed Notifications
To configure how and which Tanium notifications are shown/allowed.

## Managed Login Items
Profile to ensure users cannot disable the Tanium Login Items via System Settings.