# Reference Configuration Profiles for Tellix

Both of these profiles must be installed via MDM to devices with the Supervised status.

## Privacy_Configuration_for_Trellix_EndPoint_Security.mobileconfig

Profile to grant the various Trellix subcomponents Full Disk Access. Allow Trellix to be installed via MDM with no user interaction.

## Managed_Login_Items_Trellix.mobileconfig

**macOS Ventura only**

Profile to ensure users cannot disable the Trellix Login Items via System Settings.
